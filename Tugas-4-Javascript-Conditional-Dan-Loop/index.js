// Soal 1
var nilai;
nilai = 65;
// Jika nilai >= 85 indeksnya A
if (nilai >= 85) {
  console.log("A");
}
// Jika nilai >= 75 dan nilai < 85 indeksnya B
else if (nilai >= 75 && nilai < 85) {
  console.log("B");
}
// Jika nilai >= 65 dan nilai < 75 indeksnya c
else if (nilai >= 65 && nilai < 75) {
  console.log("C");
}
// Jika nilai >= 55 dan nilai < 65 indeksnya D
else if (nilai >= 55 && nilai < 65) {
  console.log("D");
}
// Jika nilai < 55 indeksnya E
else {
  console.log("E");
}
// -------------------------------------------------- //

// Soal 2
var tanggal = 16;
var bulan = 1;
var tahun = 2000;
switch (bulan) {
  case 1:
    bulan = "Januari";
    break;
  case 2:
    bulan = "Februari";
    break;
  case 3:
    bulan = "Maret";
    break;
  case 4:
    bulan = "April";
    break;
  case 5:
    bulan = "Mei";
    break;
  case 6:
    bulan = "Juni";
    break;
  case 7:
    bulan = "Juli";
    break;
  case 8:
    bulan = "Agustus";
    break;
  case 9:
    bulan = "September";
    break;
  case 10:
    bulan = "Oktober";
    break;
  case 11:
    bulan = "November";
    break;
  case 12:
    bulan = "Desember";
    break;
  default:
    bulan = "Bulan tidak valid";
    break;
}
console.log(tanggal + " " + bulan + " " + tahun);
// -------------------------------------------------- //

// Soal 3
// Output untuk n = 3
var n = 3;
var m = "";
for (var a = 0; a < n; a++) {
  for (var b = 0; b <= a; b++) {
    m += "#";
  }
  m += "\n";
}
console.log(m);

// Output untuk n = 7
var n = 7;
var m = "";
for (var a = 0; a < n; a++) {
  for (var b = 0; b <= a; b++) {
    m += "#";
  }
  m += "\n";
}
console.log(m);
// -------------------------------------------------- //

// Soal 4
// Output untuk m = 3
var m = 3;
var str1 = "programming";
var str2 = "Javascript";
var str3 = "VueJS";
var str4 = "===";
var a = "" + str4;
for (var i = 1; i <= m; i++) {
  if (i % 3 === 1) {
    console.log(i + " - I love " + str1);
  } else if (i % 3 === 2) {
    console.log(i + " - I love " + str2);
  } else if (i % 1 === 0) {
    console.log(i + " - I love " + str3);
    console.log(a);
  }
}

// Output untuk m = 7
var m = 7;
var str1 = "programming";
var str2 = "Javascript";
var str3 = "VueJS";
var str4 = "===";
var a = "" + str4;
for (var i = 1; i <= m; i++) {
  if (i % 3 === 1) {
    console.log(i + " - I love " + str1);
  } else if (i % 3 === 2) {
    console.log(i + " - I love " + str2);
  } else if (i % 1 === 0) {
    console.log(i + " - I love " + str3);
    console.log(a);
  }
}

// Output untuk m = 10
var m = 10;
var str1 = "programming";
var str2 = "Javascript";
var str3 = "VueJS";
var str4 = "===";
var a = "" + str4;
for (var i = 1; i <= m; i++) {
  if (i % 3 === 1) {
    console.log(i + " - I love " + str1);
  } else if (i % 3 === 2) {
    console.log(i + " - I love " + str2);
  } else if (i % 1 === 0) {
    console.log(i + " - I love " + str3);
    console.log(a);
  }
}
// -------------------------------------------------- //
