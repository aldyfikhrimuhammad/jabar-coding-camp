// Soal 1 - Function Penghitung Jumlah Kata
function jumlah_kata(kalimat) {
    var kata = kalimat.split(' ');
    var jumlah = kata.length;
    return jumlah;
}
var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = " Saya Iqbal"
var kalimat_3 = " Saya Muhammad Iqbal Mubarok "
var kata1 = kalimat_1.trim();
var kata2 = kalimat_2.trim();
var kata3 = kalimat_3.trim();
console.log(jumlah_kata(kata1)); // output : 5
console.log(jumlah_kata(kata2)); // output : 1
console.log(jumlah_kata(kata3)); // output : 5

// Soal 2 - Function Penghasil Tanggal Hari Esok
function next_date(tanggal, bulan, tahun) {
    var hari = tanggal;
    var bulan = bulan;
    var tahun = tahun;
    var hari_esok = hari + 1;
    var bulan_esok = bulan;
    var tahun_esok = tahun;
    if (hari_esok == 32) {
        hari_esok = 1;
        bulan_esok = bulan + 1;
        if (bulan_esok == 13) {
            bulan_esok = 1;
            tahun_esok = tahun + 1;
        }
    }
    if (hari_esok < 10) {
        hari_esok = hari_esok;
    }
    if (bulan_esok < 10) {
        bulan_esok = bulan_esok;
    }
    if (tahun_esok < 10) {
        tahun_esok = tahun_esok;
    }
    switch (bulan_esok) {
        case 1:
            bulan_esok = 'Januari';
            break;
        case 2:
            bulan_esok = 'Februari';
            break;
        case 3:
            bulan_esok = 'Maret';
            break;
        case 4:
            bulan_esok = 'April';
            break;
        case 5:
            bulan_esok = 'Mei';
            break;
        case 6:
            bulan_esok = 'Juni';
            break;
        case 7:
            bulan_esok = 'Juli';
            break;
        case 8:
            bulan_esok = 'Agustus';
            break;
        case 9:
            bulan_esok = 'September';
            break;
        case 10:
            bulan_esok = 'Oktober';
            break;
        case 11:
            bulan_esok = 'November';
            break;
        case 12:
            bulan_esok = 'Desember';
            break;
    }
    if (bulan_esok == 2 && hari_esok == 29) {
        hari_esok = 1;
        bulan_esok = bulan_esok + 1;
        if (bulan_esok == 13) {
            bulan_esok = 1;
            tahun_esok = tahun + 1;
        }
    }
    if (bulan_esok == 2 && hari_esok == 28) {
        hari_esok = 1;
        bulan_esok = bulan_esok + 1;
        if (bulan_esok == 13) {
            bulan_esok = 1;
            tahun_esok = tahun + 1;
        }
    }
    if (bulan_esok == 4 || bulan_esok == 6 || bulan_esok == 9 || bulan_esok == 11) {
        if (hari_esok == 31) {
            hari_esok = 1;
            bulan_esok = bulan_esok + 1;
            if (bulan_esok == 13) {
                bulan_esok = 1;
                tahun_esok = tahun + 1;
            }
        }
    }
    if (bulan_esok == 12 && hari_esok == 31) {
        hari_esok = 1;
        bulan_esok = 1;
        tahun_esok = tahun + 1;
    }

    return hari_esok + ' ' + bulan_esok + ' ' + tahun_esok;
}
// Contoh 1
var tanggal = 29;
var bulan = 2;
var tahun = 2020;
console.log(next_date(tanggal, bulan, tahun)); // output : 1 Maret 2020

// Contoh 2
var tanggal = 28;
var bulan = 2;
var tahun = 2021;
console.log(next_date(tanggal, bulan, tahun)); // output : 1 Maret 2021

// Contoh 3
var tanggal = 31;
var bulan = 12;
var tahun = 2020;

console.log(next_date(tanggal , bulan , tahun )); // output : 1 Januari 2021